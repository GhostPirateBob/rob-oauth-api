# Loaded MBA API

## API Details

### Test Domain details

- Domain:         amawa.test.rewards-plus.com.au
- Client ID:      AMAWA
- Client Secret:  O8lpQOIrcDL6zuiZm2VBR0iiwO6H03poN5PG
- Group:          AMA WA

## MBA API Account Commands

### Create

- URL:            https://amawa.test.rewards-plus.com.au/api/v1/Accounts/create
- Type:           POST Only

#### Example Request
```json
{
  "client_id": "{{client_id}}",
  "client_secret": "{{client_secret}}",
  "reference": "{{UNIQUE-CODE-01}}",
  "email": "test@email.com",
  "last_name": "Smith",
  "first_name": "AV",
  "group": "Default",
  "phone": "123456789",
  "status": "Enabled",
  "membership_expiry": "2025-31-12",
  "membership_number": "123456",
  "newsletter": true,
  "address": {
    "line1": "Unit 1000",
    "line2": "Barry St",
    "city": "Melbourne",
    "post_code": "3000",
    "state": "VIC",
    "country": "Australia"
  },
  "extra_fields": [
    {
      "name": "Title",
      "value": "Mr"
    }
  ]
}
```

#### My Request

```json
{
  "client_id": "AMAWA",
  "client_secret": "O8lpQOIrcDL6zuiZm2VBR0iiwO6H03poN5PG",
  "reference": "3b42ba9780a34883a02e5b30f5506db4",
  "email": "test@example.com",
  "last_name": "Tester",
  "first_name": "John",
  "group": "AMA WA",
  "phone": "0412345678",
  "status": "Enabled",
  "membership_expiry": "2025-31-12",
  "membership_number": "123457",
  "newsletter": true,
  "address": {
    "line1": "Test Building",
    "line2": "100 Wellington Street",
    "city": "Perth",
    "post_code": "6000",
    "state": "WA",
    "country": "Australia"
  }
}
```

#### Success

```json
{
  "status": "ok"
}
```

### Update

- URL:            https://amawa.test.rewards-plus.com.au/api/v1/Accounts/update
- Type:           POST Only

### Update Status

- URL:            https://amawa.test.rewards-plus.com.au/api/v1/Accounts/update-status
- Type:           POST Only

## MBA API SSO Commands

### Login Request

- URL:            https://amawa.test.rewards-plus.com.au/api/v1/SSO/login
- Type:           GET Only

#### My Request

```shell
curl --request GET \
  --url https://amawa.test.rewards-plus.com.au/api/v1/SSO/login \
  --header 'content-type: application/x-www-form-urlencoded' \
  --cookie 'OCSESSID=514103dbb68436edf85c1ed9a2; language=en-gb; currency=AUD; language=en-gb; currency=AUD' \
  --data grant_type=client_credentials \
  --data client_id=AMAWA \
  --data client_secret=O8lpQOIrcDL6zuiZm2VBR0iiwO6H03poN5PG \
  --data user_id=test@example.com \
  --data user_uid=3b42ba9780a34883a02e5b30f5506db4 \
  --data scope=sc
```

### Logout

- URL:            https://amawa.test.rewards-plus.com.au/api/v1/SSO/logout
- Type:           GET Only

### Login Redirect

- URL:    https://amawa.test.rewards-plus.com.au
- Type:   301 Redirect
