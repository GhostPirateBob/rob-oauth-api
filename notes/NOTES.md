# AMA WA Notes and Links

## Site URL's

### Staging

- [https://amawa.test.rewards-plus.com.au](https://amawa.test.rewards-plus.com.au)

### Development

- [https://amawa.lcprojects.com.au](https://amawa.lcprojects.com.au)
- [https://amawa.lcprojects.com.au/wp-admin](https://amawa.lcprojects.com.au/wp-admin)
- [https://amawa.lcprojects.com.au/membership/join](https://amawa.lcprojects.com.au/membership/join)

### Live

- [https://www.amawa.com.au](https://www.amawa.com.au)

## API Provider

- [http://www.mbabenefits.com.au](http://www.mbabenefits.com.au/)

## GitKraken Example Middleware

https://api.gitkraken.com/oauth/gitlab/callback?code=ca81fce25debcd8f2305bbcbd6f0d5b1e1d3c51c5c556585f68cf521d819df11&state=socketId%3A90467c1a-b8fa-46a8-b858-3ac05e55e73b%3Baction%3Aauthorize%3BinApp%3Atrue%3BneedsGKPermissions%3Atrue%3Bchallenge%3Afaa12a95-3571-441f-8e07-966caa814ed4%3B

## NPM/Composer Modules

  - php curl: [https://github.com/guzzle/guzzle](https://github.com/guzzle/guzzle)
  - php oauth1: [https://github.com/thephpleague/oauth1-client](https://github.com/thephpleague/oauth1-client)
  - php oauth2: [https://github.com/thephpleague/oauth2-client](https://github.com/thephpleague/oauth2-client)
  - js curl: [https://www.npmjs.com/package/axios](https://www.npmjs.com/package/axios)
  - js oauth: [https://www.npmjs.com/package/oauth](https://www.npmjs.com/package/oauth)

## Goole OAuth2 Documentation

  - [https://developers.google.com/identity/protocols/oauth2](https://developers.google.com/identity/protocols/oauth2)

## Email Notes

Please find below details for you to implement the APIs for SSO and member account maintenance.
 
Test API details are:
https://amawa.test.rewards-plus.com.au/
Client Id: AMAWA
Client Secret: O8lpQOIrcDL6zuiZm2VBR0iiwO6H03poN5PG
Group: AMA WA
 
Please find a link to the API documentation here https://static.mbaprd1.com/IT/APIs/
 
Can you also please details for the configuration:
URL redirect on Logout – upon member logout we can display a confirmation message on the portal or we can redirect to a URL. Please provide which option and provide URL if selecting this option
Login URL – If a member visits the portal without logging in, please indicate which URL we should redirect them to log in on your site for them to be redirected back to the portal as a logged-in member
 
Once you have completed your development work, we will provide some test cases to simulate different scenarios and cross-check that the data received on our side is as you intended. Once this is successful we will provide you with the final live production credentials and replicate the tests before going live.
