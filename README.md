# rob-oauth-api

## Instructions

Assorted nodejs/php code for amawa's oauth client

See the "/notes" folder for API notes and pdf/email attachments

Most of the token handling is performed in "/src/php/index.php" currently

Comment out the header() and exit calls from "index.php" to stop yourself getting redirected as soon as the page loads

```
if (!isset($_GET['code'])) {
  ...
  // header('Location: ' . authorizationUrl);
  // exit;
}
```

### Current test URL

- [https://amawa.lcprojects.com.au/oauth](https://amawa.lcprojects.com.au/oauth/?foo=something&bar=123456789)

### Uses the following modules

- composer.json php modules:

  - curl: ["guzzlehttp/guzzle": "^7.0"](https://github.com/guzzle/guzzle)
  - oauth1: ["league/oauth1-client": "^2.5"](https://github.com/thephpleague/oauth1-client)
  - oauth2: ["league/oauth2-client": "^2.5"](https://github.com/thephpleague/oauth2-client)

- package.json npm js modules:

  - curl: ["axios": "^0.20.0"](https://www.npmjs.com/package/axios)
  - oauth: ["oauth": "^0.9.15"](https://www.npmjs.com/package/oauth)

## Requirements

- nodejs v10.16.0 
- php-7.2/composer

## Install Instructions

```git clone "https://gitlab.com/GhostPirateBob/rob-oauth-api.git"```

```cd rob-oauth-api```

```npm install```

```cd src/php```

```composer install```

## NPM Commands

```npm run start```

- Runs "nodejs src/js/oauth.js" for node testing

```npm run build```

- (Currently does nothing)

## Authorization Page

![Authorization Page Preview](https://s3.ap-southeast-2.amazonaws.com/ghostpirates/img/rob-oauth-api-page-preview.png "Authorization Page Preview")

- Early preview image of the "Authorization Page" with full debug messages

## Site URL's

### Dev

- [https://amawa.lcprojects.com.au/oauth](https://amawa.lcprojects.com.au/oauth/?foo=something&bar=123456789)

### Staging

- [https://amawa.test.rewards-plus.com.au](https://amawa.test.rewards-plus.com.au)

### Live

- [https://www.amawa.com.au](https://www.amawa.com.au)

## API Provider

- [http://www.mbabenefits.com.au](http://www.mbabenefits.com.au/)

