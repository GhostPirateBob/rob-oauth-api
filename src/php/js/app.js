// basic fetch api example
const request = new Request('https://amawa.lcprojects.com.au/oauth/img/ama-favicon.svg');
const url = request.url;
const method = request.method;
const credentials = request.credentials;

fetch(request)
	.then(response => response.text())
	.then((response) => {
			// console.log("updated request below");
			// console.log(request);
			console.log("got response from fetch! - full response below");
			console.log(response.slice(0, 50));
	})
	.catch(err => console.log(err));
