// Licensed under a CC0 1.0 Universal (CC0 1.0) Public Domain Dedication
// http://creativecommons.org/publicdomain/zero/1.0/

// HTML files: try the network first, then the cache.
// Other files: try the cache first, then the network.
// Both: cache a fresh version if possible.
// (beware: the cache will grow and grow; there's no cleanup)

const cacheName = 'files';
console.log('greetings from the service worker');

addEventListener('fetch',  fetchEvent => {
  const request = fetchEvent.request;
  if (request.method !== 'GET') {
    console.log('request.method NOT === GET exiting...');
    return;
  }
  fetchEvent.respondWith(async function() {
    const fetchPromise = fetch(request);
    console.log('sw fetchPromise');
    fetchEvent.waitUntil(async function() {
      const responseFromFetch = await fetchPromise;
      const responseCopy = responseFromFetch.clone();
      const myCache = await caches.open(cacheName);
      return myCache.put(request, responseCopy);
    }());
    if (request.headers.get('Accept').includes('text/html')) {
      try {
        return await fetchPromise;
      }
      catch(error) {
        console.log(error);
        return caches.match(request);
      }
    } else {
      console.log('responseFromCache');
      const responseFromCache = await caches.match(request);
      return responseFromCache || fetchPromise;
    }
  }());
});

// console.log('running fetch from service-worker!');

// basic fetch api example
// const request = new Request('https://amawa.lcprojects.com.au/oauth/img/ama-favicon.svg');
// const url = request.url;
// const method = request.method;
// const credentials = request.credentials;

// fetch(request)
// 	.then(response => response.text())
// 	.then((response) => {
// 			// console.log("updated request below");
// 			// console.log(request);
// 			console.log("got response from fetch! - full response below");
// 			console.log(response.slice(0, 50));
// 	})
// 	.catch(err => console.log(err));
