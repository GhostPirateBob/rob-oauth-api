<?php

require_once __DIR__.'/../vendor/autoload.php';

$_GET['oauth_token'] = "O8lpQOIrcDL6zuiZm2VBR0iiwO6H03poN5PG";
$_GET['oauth_verifier'] = "https://amawa.test.rewards-plus.com.au/api/v1/SSO/login";
// $_GET['user'] = "test@example.com";

	// Create server
$server = new League\OAuth1\Client\Server\Twitter(array(
	'identifier' => 'AMAWA',
	'secret' => 'O8lpQOIrcDL6zuiZm2VBR0iiwO6H03poN5PG',
	'callback_uri' => "http://amawa.lcprojects.com.au/oauth/oauth1/callback.php",
));

// Start session
session_start();
?><!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title> oauth1-api </title>
	<meta name="author" content="">
	<meta name="description" content="oauth1-api test page">
	<meta name="keywords" content="oauth1,api">
	<meta name="theme-color" content="#253052">
	<link rel='manifest' href='/oauth/manifest.json'>
	<link rel="icon" href="/oauth/img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="/oauth/img/apple-touch-icon.png">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Space+Mono&display=swap" type="text/css" async>
	<link rel="stylesheet" href="/oauth/css/normalize.min.css" type="text/css">
	<link rel="stylesheet" href="/oauth/css/bootstrap-grid.min.css" type="text/css">
	<link rel="stylesheet" href="/oauth/css/styles.css" type="text/css">
	</head>
	<body>
	<div id="main" class="container mb-5 mt-5">
		<h1 class="text-center h3"> OAuth1 Test Page </h1>
		<div class="row d-flex align-items-center justify-content-center">
			<div class="col-auto">
<?php echo '<a class="btn-primary" href="?go=go">Login</a>'; ?>
			</div>
		</div>
		<div class="row d-flex align-items-center justify-content-center">
			<div class="col-12">
				<pre class="var-dump">
					<h3>$_SESSION</h3>
<?php highlight_string("<?php \$data = " . var_export($_SESSION, true) . "; ?>"); ?>
				</pre>
			</div>
			<div class="col-12">
				<pre class="var-dump">
					<h3>$_REQUEST</h3>
<?php highlight_string("<?php \$data = " . var_export($_REQUEST, true) . "; ?>"); ?>
				</pre>
			</div>
			<div class="col-12">
				<pre class="var-dump">
					<h3>$_SERVER</h3>
<?php highlight_string("<?php \$data = " . var_export($_SERVER, true) . "; ?>"); ?>
				</pre>
			</div>
			<?php
// Step 4
if (isset($_GET['user'])) {
	// Check somebody hasn't manually entered this URL in,
	// by checking that we have the token credentials in
	// the session.
	if ( ! isset($_SESSION['token_credentials'])) {
		echo 'No token credentials.';
		// exit(1);
	}

	// Retrieve our token credentials. From here, it's play time!
	$tokenCredentials = unserialize($_SESSION['token_credentials']);

	// // Below is an example of retrieving the identifier & secret
	// // (formally known as access token key & secret in earlier
	// // OAuth 1.0 specs).
	// $identifier = $tokenCredentials->getIdentifier();
	// $secret = $tokenCredentials->getSecret();

	// Some OAuth clients try to act as an API wrapper for
	// the server and it's API. We don't. This is what you
	// get - the ability to access basic information. If
	// you want to get fancy, you should be grabbing a
	// package for interacting with the APIs, by using
	// the identifier & secret that this package was
	// designed to retrieve for you. But, for fun,
	// here's basic user information.
	$user = $server->getUserDetails($tokenCredentials);
	var_dump($user);

// Step 3
} elseif (isset($_GET['oauth_token']) && isset($_GET['oauth_verifier'])) {

	// Retrieve the temporary credentials from step 2
	$temporaryCredentials = unserialize($_SESSION['temporary_credentials']);

	// Third and final part to OAuth 1.0 authentication is to retrieve token
	// credentials (formally known as access tokens in earlier OAuth 1.0
	// specs).
	$tokenCredentials = $server->getTokenCredentials($temporaryCredentials, $_GET['oauth_token'], $_GET['oauth_verifier']);

	// Now, we'll store the token credentials and discard the temporary
	// ones - they're irrelevant at this stage.
	unset($_SESSION['temporary_credentials']);
	$_SESSION['token_credentials'] = serialize($tokenCredentials);
	// session_write_close();

	// Redirect to the user page
	// var_dump("Location: http://{$_SERVER['HTTP_HOST']}/?user=user");
	// header("Location: http://{$_SERVER['HTTP_HOST']}/?user=user");
	// exit;

// Step 2.5 - denied request to authorize client
} elseif (isset($_GET['denied'])) {
	echo 'Hey! You denied the client access to your Twitter account! If you did this by mistake, you should <a href="?go=go">try again</a>.';

// Step 2
} elseif (isset($_GET['go'])) {

	// First part of OAuth 1.0 authentication is retrieving temporary credentials.
	// These identify you as a client to the server.
	$temporaryCredentials = $server->getTemporaryCredentials();

	// Store the credentials in the session.
	$_SESSION['temporary_credentials'] = serialize($temporaryCredentials);
	// session_write_close();

	// Second part of OAuth 1.0 authentication is to redirect the
	// resource owner to the login screen on the server.
	$server->authorize($temporaryCredentials);
}
?>
<?php if ( $temporaryCredentials ) { ?>
			<div class="col">
				<pre class="var-dump">
					<h3>$temporaryCredentials</h3>
<?php highlight_string("<?php \$data = " . var_export($temporaryCredentials, true) . "; ?>"); ?>
				</pre>
			</div>
<?php } ?>
		</div>
	</div>
	<!-- <script src="/oauth/js/jquery.min.js" type="text/javascript"></script> -->
	<!-- <script src="/oauth/js/app.js" type="text/javascript"></script> -->
	</body>
</html>
<?php 
exit();
