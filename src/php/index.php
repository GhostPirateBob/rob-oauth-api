<?php
	// require composer dependencies
	// if ( !file_exists(__DIR__ . '/vendor/autoload.php') ) {
	// 	echo "error: composer autoload.php not found, run \"composer install\" to generate the /vendor folder";
	// 	exit(0);
	// }
	require_once __DIR__ . '/vendor/autoload.php';
	use GuzzleHttp\Client;
	use League\OAuth2;
	$client = new Client([
		// Base URI is used with relative requests
		'base_uri' => 'https://amawa.test.rewards-plus.com.au',
		// You can set any number of default request options.
		'timeout'  => 2.0,
	]);
	$provider = new \League\OAuth2\Client\Provider\GenericProvider([
	'clientId'                => 'AMAWA',
	'clientSecret'            => 'O8lpQOIrcDL6zuiZm2VBR0iiwO6H03poN5PG',
	'redirectUri'             => 'https://amawa.lcprojects.com.au/oauth/redirect/',
	'urlAuthorize'            => 'https://amawa.test.rewards-plus.com.au/api/v1/SSO/login',
	'urlAccessToken'          => 'https://amawa.test.rewards-plus.com.au/api/v1/SSO/login',
	'urlResourceOwnerDetails' => 'https://amawa.test.rewards-plus.com.au/api/v1/SSO/login'
]);
// If we don't have an authorization code then get one
if (!isset($_GET['code'])) {
	// Fetch the authorization URL from the provider; this returns the
	// urlAuthorize option and generates and applies any necessary parameters
	// (e.g. state).
	$authorizationUrl = $provider->getAuthorizationUrl();
	// Get the state generated for you and store it to the session.
	$_SESSION['oauth2state'] = $provider->getState();
	// Redirect the user to the authorization URL.
	// header('Location: ' . $authorizationUrl);
	// exit;
// Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {
	if (isset($_SESSION['oauth2state'])) {
		unset($_SESSION['oauth2state']);
	}
	// exit('Invalid state');
} else {
	try {
		// Try to get an access token using the authorization code grant.
		$accessToken = $provider->getAccessToken('authorization_code', [
			// 'code' => $_GET['code'],
			'code' => 'O8lpQOIrcDL6zuiZm2VBR0iiwO6H03poN5PG',
			'grant_type' => 'client_credentials',
			'client_id' => 'AMAWA',
			'client_secret' => 'O8lpQOIrcDL6zuiZm2VBR0iiwO6H03poN5PG',
			'user_id' => 'test@example.com',
			'user_uid' => '3b42ba9780a34883a02e5b30f5506db4',
			'scope' => 'sc'
		]);
		// We have an access token, which we may use in authenticated
		// requests against the service provider's API.
		echo 'Access Token: ' . $accessToken->getToken() . "<br>";
		echo 'Refresh Token: ' . $accessToken->getRefreshToken() . "<br>";
		echo 'Expired in: ' . $accessToken->getExpires() . "<br>";
		echo 'Already expired? ' . ($accessToken->hasExpired() ? 'expired' : 'not expired') . "<br>";
		// Using the access token, we may look up details about the
		// resource owner.
		$resourceOwner = $provider->getResourceOwner($accessToken);
		var_export($resourceOwner->toArray());
		// The provider provides a way to get an authenticated API request for
		// the service, using the access token; it returns an object conforming
		// to Psr\Http\Message\RequestInterface.
		$request = $provider->getAuthenticatedRequest(
			'GET',
			'https://amawa.test.rewards-plus.com.au/api/v1/SSO/login',
			$accessToken
		);
	} catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
		// Failed to get the access token or user details.
		exit($e->getMessage());
	}
}
?><!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title> oauth-api </title>
	<meta name="author" content="">
	<meta name="description" content="oauth-api test page">
	<meta name="keywords" content="oauth,api">
	<meta name="theme-color" content="#253052">
	<link rel='manifest' href='/oauth/manifest.json'>
	<!-- <script type="module" src="/oauth/js/sw.js"></script> -->
	<link rel="icon" href="/oauth/img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="/oauth/img/apple-touch-icon.png">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Space+Mono&display=swap" type="text/css" async>
	<link rel="stylesheet" href="/oauth/css/normalize.min.css" type="text/css">
	<link rel="stylesheet" href="/oauth/css/bootstrap-grid.min.css" type="text/css">
	<link rel="stylesheet" href="/oauth/css/styles.css" type="text/css">
	</head>
	<body>
	<div id="main" class="container mb-5 mt-5">
		<h1 class="text-center h2"> OAuth "Authorization Page" Test </h1>
<?php if ( $_REQUEST && is_array($_REQUEST) ) { ?>
		<div class="row d-flex align-items-center">
		<div class="col-auto col-dump-label">
			<p class="text-bold">
				<span class="text-success">✔</span>
				<span> Incoming $_GET / $_POST Params </span>
			</p>
		</div>
		<div class="col">
			<pre class="var-dump">
<?php highlight_string("<?php \$data = " . var_export($_REQUEST, true) . "; ?>"); ?>
			</pre>
		</div>
		</div>
<?php } ?>
<?php if ( $authorizationUrl && strlen( $authorizationUrl ) > 2 ) { ?>
		<div class="row d-flex align-items-center">
		<div class="col-auto col-dump-label">
			<p class="text-bold">
				<span class="text-success">✔</span>
				<span> Intercepted "Header Location:" URL </span>
			</p>
		</div>
		<div class="col">
			<p class="text-monospace var-dump">Header Location: <a href="<?php echo urldecode($authorizationUrl); ?>" target="_blank" rel="noopener">
			<?php echo urldecode($authorizationUrl); ?></a></p>
		</div>
		</div>
		<div class="row d-flex align-items-center">
		<div class="col-auto col-dump-label">
			<p class="text-bold">
				<span class="text-success">✔</span>
				<span> Intercepted "Header Location:" Params </span>
			</p>
		</div>
		<div class="col">
			<pre class="var-dump">
<?php highlight_string("<?php \$data = " . var_export(parse_url(urldecode($authorizationUrl)), true) . "; ?>"); ?>
			</pre>
		</div>
		</div>
		<div class="row d-flex align-items-center">
		<div class="col-auto col-dump-label">
			<p class="text-bold">
				<span class="text-success">✔</span>
				<span> OAuth2 "Authorization URL" Params </span>
			</p>
		</div>
		<div class="col">
			<pre class="var-dump">
<?php highlight_string("<?php \$data = " . var_export(parse_url(urldecode($authorizationUrl)), true) . "; ?>"); ?>
			</pre>
		</div>
		</div>
<?php } ?>
		<div class="row mt-5">
			<div class="col-12 col-lg-5 d-flex flex-column">
				<h2 class="text-center mt-0 mb-3 h3"> php Guzzle $client data </h2>
				<pre class="var-dump">
<?php highlight_string("<?php \$data = " . var_export($client, true) . "; ?>"); ?>
				</pre>
			</div>
			<div class="col-12 col-lg-7 d-flex flex-column">
				<h2 class="text-center mt-0 mb-3 h3"> php OAuth2 $provider data </h2>
				<pre class="var-dump">
<?php highlight_string("<?php \$data = " . var_export($provider, true) . "; ?>"); ?>
				</pre>
			</div>
			<div class="col">
				<pre class="var-dump">
					<h3>$_SERVER</h3>
<?php highlight_string("<?php \$data = " . var_export($_SERVER, true) . "; ?>"); ?>
				</pre>
			</div>
		</div>
	</div>
	<script src="/oauth/js/jquery.min.js" type="text/javascript"></script>
	<script src="/oauth/js/app.js" type="text/javascript"></script>
	</body>
</html>
